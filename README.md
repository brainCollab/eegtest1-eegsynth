# EEGtest1-eegsynth
This is an [eegsynth](https://github.com/eegsynth/eegsynth) patch for a neurofeedback demo through LSL. See the [eegsynth website](https://github.com/eegsynth/eegsynth) for installation instructions. 

How to start this patch :

    cd patch
    eegsynth lsl2ft.ini preprocessing.ini spectral.ini plotcontrol.ini historycontrol.ini postprocessing.ini postprocessing-2.ini outputlsl.ini inputcontrol.ini plotsignal.ini 

How to operate the patch : 
- A baseline signal is acquired as long as the button03 is toggled to "on". This baseline computes a moving average and std of the 3 first EEG channels. 
- This baseline is acquired while the subject is instructed to perform a task with significant cognitive load (e.g. counting backwards in steps of 7 starting from 93, or thinking about names of tools)
- After the end of baseline acquisition, the subject is instructed to relax, think about nothing particular, similar to a meditation state. 
- The ratio alpha / beta is computed (after having normalized alpha and beta power with the avg and std of the baseline interval), and the normalized ratio is sent via lsl


## License
MIT License
